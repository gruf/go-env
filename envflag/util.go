package envflag

import (
	"flag"
	flags "flag"
	"fmt"
	"os"
	"reflect"
	"strconv"
	"strings"
	_ "unsafe" // linkname to flag.isZeroValue()

	"codeberg.org/gruf/go-bytes"
)

// usage will print usage information for provided flagset.
func usage(set *flag.FlagSet) {
	buf := bytes.Buffer{}
	fmt.Fprintf(&buf, "Usage: %s ...\n", os.Args[0])
	defaults(flags.CommandLine, &buf)
	set.Output().Write(buf.B)
}

// defaults outputs formatted arguments defaults from FlagSet into Buffer.
func defaults(set *flag.FlagSet, buf *bytes.Buffer) {
	var isSet bool

	// flag information for the '-help' flag (is a default)
	const helpstr = "  -help\n    \tPrint usage information\n"

	set.VisitAll(func(flag *flags.Flag) {
		// Add a help line if none found
		if flag.Name == "help" {
			isSet = true
		} else if !isSet && "help" < flag.Name {
			buf.WriteString(helpstr)
			isSet = true
		}

		// Two spaces before -; see next two comments.
		buf.WriteString("  -" + flag.Name)
		if vt := simpletype(flag.Value); vt != "" {
			buf.WriteString(" " + vt)
		}

		// Boolean flags of one ASCII letter are so common we
		// treat them specially, putting their usage on the same line.
		if buf.Len() <= 4 { // space, space, '-', 'x'.
			buf.WriteString("\t")
		} else {
			// Four spaces before the tab triggers good alignment
			// for both 4- and 8-space tab stops.
			buf.WriteString("\n    \t")
		}

		// Write usage with formatted new lines
		buf.WriteString(strings.ReplaceAll(
			flag.Usage,
			"\n",
			"\n    \t",
		))

		if !isZeroValue(flag, flag.DefValue) {
			// Append default value if non-zero
			buf.WriteString(" (default ")
			if !strconv.CanBackquote(flag.DefValue) {
				buf.WriteString(strconv.Quote(flag.DefValue))
			} else {
				buf.WriteString(flag.DefValue)
			}
			buf.WriteByte(')')
		}

		// Final newline
		buf.WriteByte('\n')
	})

	if !isSet /* help was not set */ {
		buf.WriteString(helpstr)
	}
}

// simpletype returns a simplified type string for 'v'.
func simpletype(v interface{}) string {
	t := reflect.TypeOf(v)
	k := t.Kind()
	for k == reflect.Ptr {
		t = t.Elem()
		k = t.Kind()
	}
	if k == reflect.Bool ||
		k == reflect.Func {
		return ""
	}
	return k.String()
}

//go:linkname isZeroValue flag.isZeroValue
func isZeroValue(flag *flags.Flag, value string) bool
