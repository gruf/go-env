package util

import (
	"errors"
	"strings"
	"unicode"
	"unicode/utf8"
)

// SplitCommaSeparated splits a comma separated (handles each elem being quoted, and delimitting).
func SplitCommaSeparated(str string, each func(string) error) error {
	for len(str) > 0 {
		if str[0] == '\'' || str[0] == '"' {
			// Look for next quote
			i := 1 + nextDelim(str[1:], str[0])
			if i == -1 {
				return errors.New("missing end quote")
			}

			// Append next element
			if err := each(str[1:i]); err != nil {
				return err
			}

			// Reslice + trim leading space
			str = trimLeadingSpace(str[i+1:])

			if len(str) < 1 {
				// Reached end
				break
			}

			// MUST have comma
			if str[0] != ',' {
				return errors.New("missing comma separator")
			}

			// Reslice + trim leading space
			str = trimLeadingSpace(str[1:])
			continue
		}

		// Look for next comma
		i := nextDelim(str, ',')
		if i == -1 {
			return each(str)
		}

		// Append next element
		if err := each(str[:i]); err != nil {
			return err
		}

		// Reslice + trim leading space
		str = trimLeadingSpace(str[i+1:])
	}

	return nil
}

// asciiSpace is a lookup table of ascii space chars.
var asciiSpace = [256]uint8{
	'\t': 1,
	'\n': 1,
	'\v': 1,
	'\f': 1,
	'\r': 1,
	' ':  1,
}

// trimLeadingSpace trims the leading space from a string.
func trimLeadingSpace(str string) string {
	start := 0
	for ; start < len(str); start++ {
		c := str[start]
		if c >= utf8.RuneSelf {
			return strings.TrimFunc(str[start:], unicode.IsSpace)
		}
		if asciiSpace[c] == 0 {
			break
		}
	}
	return str[start:]
}

// nextDelim finds the next delimited instance of 'c' in 'str'.
func nextDelim(str string, c byte) int {
	var delim bool
	for i := range str {
		switch str[i] {
		case '\\':
			delim = !delim
		case c:
			if !delim {
				return i
			}
			fallthrough
		default:
			delim = false
		}
	}
	return -1
}
