# go-env

Simple wrappers round `syscall.Getenv()` and `syscall.Setenv()` functions to provide easy getting / setting of environment variables of various types.