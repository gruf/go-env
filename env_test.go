package env_test

import (
	"os"
	"strconv"
	"strings"
	"testing"
	"time"

	"codeberg.org/gruf/go-bytesize"
	"codeberg.org/gruf/go-env"
)

var (
	stringArray = []string{
		"hello",
		"world",
		`weirdly \"quoted\" string`,
		`string "with" quotes`,
	}
	stringArrayStr = `["hello", "world", 'weirdly \"quoted\" string', string "with" quotes]`

	boolArray = []struct {
		Value  string
		Expect bool
	}{
		{
			Value:  "true",
			Expect: true,
		},
		{
			Value:  "false",
			Expect: false,
		},
		{
			Value:  "1",
			Expect: true,
		},
		{
			Value:  "0",
			Expect: false,
		},
		{
			Value:  "True",
			Expect: true,
		},
		{
			Value:  "TRUE",
			Expect: true,
		},
		{
			Value:  "False",
			Expect: false,
		},
		{
			Value:  "FALSE",
			Expect: false,
		},
		{
			Value:  "t",
			Expect: true,
		},
		{
			Value:  "T",
			Expect: true,
		},
		{
			Value:  "f",
			Expect: false,
		},
		{
			Value:  "F",
			Expect: false,
		},
	}
	boolArrayStr = `["true", 'false', "1", 0, "True", TRUE, "False", FALSE, 't', 'T', 'f', 'F']`

	int64Array = []struct {
		Value  string
		Expect int64
	}{
		{
			Value:  "0",
			Expect: 0,
		},
		{
			Value:  "69",
			Expect: 69,
		},
		{
			Value:  "420",
			Expect: 420,
		},
		{
			Value:  "9223372036854775807",
			Expect: 9223372036854775807,
		},
		{
			Value:  "-9223372036854775807",
			Expect: -9223372036854775807,
		},
	}
	int64ArrayStr = `[0, '69', "420", 9223372036854775807, -9223372036854775807]`

	uint64Array = []struct {
		Value  string
		Expect uint64
	}{
		{
			Value:  "0",
			Expect: 0,
		},
		{
			Value:  "69",
			Expect: 69,
		},
		{
			Value:  "420",
			Expect: 420,
		},
		{
			Value:  "18446744073709551615",
			Expect: 18446744073709551615,
		},
	}
	uint64ArrayStr = `[0, "69", '420', "18446744073709551615"]`

	floatArray = []struct {
		Value  string
		Expect float64
	}{
		{
			Value:  "0",
			Expect: 0,
		},
		{
			Value:  "-1",
			Expect: -1,
		},
		{
			Value:  "1.69",
			Expect: 1.69,
		},
		{
			Value:  "1e10",
			Expect: 1e10,
		},
		{
			Value:  "-9.99999999999",
			Expect: -9.99999999999,
		},
		{
			Value:  "9.99999999999",
			Expect: 9.99999999999,
		},
	}

	durationArray = []struct {
		Value  string
		Expect time.Duration
	}{
		{
			Value:  "30s",
			Expect: time.Second * 30,
		},
		{
			Value:  "30m",
			Expect: time.Minute * 30,
		},
		{
			Value:  "30h",
			Expect: time.Hour * 30,
		},
		{
			Value:  "30h30m30s",
			Expect: 30 * (time.Second + time.Minute + time.Hour),
		},
		{
			Value:  "30s30m30h",
			Expect: 30 * (time.Second + time.Minute + time.Hour),
		},
		{
			Value:  "10s10s10s",
			Expect: 30 * time.Second,
		},
	}
	durationArrayStr = `['30s', 30m, "30h", '30h30m30s', 30s30m30h, "10s10s10s"]`

	timeArray = []struct {
		Value  string
		Expect time.Time
	}{
		{
			Value:  "Fri, 18 Feb 2022 14:44:44 GMT",
			Expect: parsetime("Fri, 18 Feb 2022 14:44:44 GMT"),
		},
		{
			Value:  "Tue, 19 Jan 2038 03:14:07 GMT",
			Expect: parsetime("Tue, 19 Jan 2038 03:14:07 GMT"),
		},
	}
	timeArrayStr = `["Fri, 18 Feb 2022 14:44:44 GMT", 'Tue, 19 Jan 2038 03:14:07 GMT']`

	sizeArray = []struct {
		Value  string
		Expect bytesize.Size
	}{
		{
			Value:  "1kiB",
			Expect: 1024,
		},
		{
			Value:  "1MiB",
			Expect: 1024 * 1024,
		},
		{
			Value:  "1GiB",
			Expect: 1024 * 1024 * 1024,
		},
		{
			Value:  "1TiB",
			Expect: 1024 * 1024 * 1024 * 1024,
		},
		{
			Value:  "1kB",
			Expect: 1000,
		},
		{
			Value:  "1MB",
			Expect: 1000000,
		},
		{
			Value:  "1GB",
			Expect: 1000000000,
		},
		{
			Value:  "1TB",
			Expect: 1000000000000,
		},
		{
			Value:  "1024B",
			Expect: 1024,
		},
	}
	sizeArrayStr = `[1kiB, 1MiB, "1GiB", '1TiB', 1kB, '1MB', 1GB, 1TB, 1024B]`
)

func TestGet(t *testing.T) {
	for _, str := range stringArray {
		_ = os.Setenv("STRING", str)
		if env.Get("STRING") != str {
			t.Fatal("env var not as expected")
		}
	}
}

func TestGetBool(t *testing.T) {
	for _, test := range boolArray {
		_ = os.Setenv("BOOL", test.Value)
		if env.GetBool("BOOL") != test.Expect {
			t.Fatal("env var not as expected")
		}
	}
}

func TestInt64(t *testing.T) {
	for _, test := range int64Array {
		_ = os.Setenv("INT64", test.Value)
		if env.GetInt64("INT64") != test.Expect {
			t.Fatal("env var not as expected")
		}
	}
}

func TestUint64(t *testing.T) {
	for _, test := range uint64Array {
		_ = os.Setenv("UINT64", test.Value)
		if env.GetUint64("UINT64") != test.Expect {
			t.Fatal("env var not as expected")
		}
	}
}

func TestFloat(t *testing.T) {
	for _, test := range floatArray {
		_ = os.Setenv("FLOAT", test.Value)
		if env.GetFloat("FLOAT") != test.Expect {
			t.Fatal("env var not as expected")
		}
	}
}

func TestDuration(t *testing.T) {
	for _, test := range durationArray {
		_ = os.Setenv("DURATION", test.Value)
		if env.GetDuration("DURATION") != test.Expect {
			t.Fatal("env var not as expected")
		}
	}
}

func TestGetTime(t *testing.T) {
	for _, test := range timeArray {
		_ = os.Setenv("TIME", test.Value)
		if env.GetTime("TIME") != test.Expect {
			t.Fatal("env var not as expected")
		}
	}
}

func TestGetSize(t *testing.T) {
	for _, test := range sizeArray {
		_ = os.Setenv("SIZE", test.Value)
		if env.GetSize("SIZE") != test.Expect {
			t.Fatalf("env var not as expected")
		}
	}
}

func TestGetBytes(t *testing.T) {
	for _, str := range stringArray {
		_ = os.Setenv("BYTES", str)
		if string(env.GetBytes("BYTES")) != str {
			t.Fatalf("env var not as expected")
		}
	}
}

func TestGetArray(t *testing.T) {
	_ = os.Setenv("STRING_ARRAY", stringArrayStr)
	arr := env.GetArray("STRING_ARRAY")
	if len(arr) != len(stringArray) {
		t.Fatalf("env var not as expected")
	}
	for i := range arr {
		if arr[i] != stringArray[i] {
			t.Fatalf("env var not as expected")
		}
	}
}

func TestGetBoolArray(t *testing.T) {
	_ = os.Setenv("BOOL_ARRAY", boolArrayStr)
	arr := env.GetBoolArray("BOOL_ARRAY")
	if len(arr) != len(boolArray) {
		t.Fatalf("env var not as expected")
	}
	for i := range arr {
		if arr[i] != boolArray[i].Expect {
			t.Fatalf("env var not as expected")
		}
	}
}

func TestGetInt64Array(t *testing.T) {
	_ = os.Setenv("INT64_ARRAY", int64ArrayStr)
	arr := env.GetInt64Array("INT64_ARRAY")
	if len(arr) != len(int64Array) {
		t.Fatalf("env var not as expected")
	}
	for i := range arr {
		if arr[i] != int64Array[i].Expect {
			t.Fatalf("env var not as expected")
		}
	}
}

func TestGetUint64Array(t *testing.T) {
	_ = os.Setenv("UINT64_ARRAY", uint64ArrayStr)
	arr := env.GetUint64Array("UINT64_ARRAY")
	if len(arr) != len(uint64Array) {
		t.Fatalf("env var not as expected")
	}
	for i := range arr {
		if arr[i] != uint64Array[i].Expect {
			t.Fatalf("env var not as expected")
		}
	}
}

func TestGetDurationArray(t *testing.T) {
	_ = os.Setenv("DURATION_ARRAY", durationArrayStr)
	arr := env.GetDurationArray("DURATION_ARRAY")
	if len(arr) != len(durationArray) {
		t.Fatalf("env var not as expected")
	}
	for i := range arr {
		if arr[i] != durationArray[i].Expect {
			t.Fatalf("env var not as expected")
		}
	}
}

func TestGetTimeArray(t *testing.T) {
	_ = os.Setenv("TIME_ARRAY", timeArrayStr)
	arr := env.GetTimeArray("TIME_ARRAY")
	if len(arr) != len(timeArray) {
		t.Fatalf("env var not as expected")
	}
	for i := range arr {
		if arr[i] != timeArray[i].Expect {
			t.Fatalf("env var not as expected")
		}
	}
}

func TestGetSizeArray(t *testing.T) {
	_ = os.Setenv("SIZE_ARRAY", sizeArrayStr)
	arr := env.GetSizeArray("SIZE_ARRAY")
	if len(arr) != len(sizeArray) {
		t.Fatalf("env var not as expected")
	}
	for i := range arr {
		if arr[i] != sizeArray[i].Expect {
			t.Fatalf("env var not as expected")
		}
	}
}

func TestSet(t *testing.T) {
	for _, str := range stringArray {
		env.Set("STRING", str)
		if os.Getenv("STRING") != str {
			t.Fatalf("env var not as expected")
		}
	}
}

func TestSetBool(t *testing.T) {
	for _, test := range boolArray {
		env.SetBool("BOOL", test.Expect)
		if os.Getenv("BOOL") != strconv.FormatBool(test.Expect) {
			t.Fatalf("env var not as expected")
		}
	}
}

func TestSetInt64(t *testing.T) {
	for _, test := range int64Array {
		env.SetInt64("INT64", test.Expect)
		if os.Getenv("INT64") != test.Value {
			t.Fatalf("env var not as expected")
		}
	}
}

func TestSetUint64(t *testing.T) {
	for _, test := range uint64Array {
		env.SetUint64("UINT64", test.Expect)
		if os.Getenv("UINT64") != test.Value {
			t.Fatalf("env var not as expected")
		}
	}
}

func TestSetFloat(t *testing.T) {
	for _, test := range floatArray {
		env.SetFloat("FLOAT", test.Expect)
		if os.Getenv("FLOAT") != strconv.FormatFloat(test.Expect, 'f', -1, 64) {
			t.Fatalf("env var not as expected")
		}
	}
}

func TestSetDuration(t *testing.T) {
	for _, test := range durationArray {
		env.SetDuration("DURATION", test.Expect)
		if os.Getenv("DURATION") != test.Expect.String() {
			t.Fatalf("env var not as expected")
		}
	}
}

func TestSetTime(t *testing.T) {
	for _, test := range timeArray {
		env.SetTime("TIME", test.Expect)
		if os.Getenv("TIME") != test.Expect.Format(time.RFC1123) {
			t.Fatalf("env var not as expected")
		}
	}
}

func TestSetSize(t *testing.T) {
	for _, test := range sizeArray {
		env.SetSize("SIZE", test.Expect)
		if os.Getenv("SIZE") != test.Expect.StringIEC() {
			t.Fatalf("env var not as expected")
		}
	}
}

func TestSetBytes(t *testing.T) {
	for _, str := range stringArray {
		env.SetBytes("BYTES", []byte(str))
		if os.Getenv("BYTES") != str {
			t.Fatalf("env var not as expected")
		}
	}
}

func TestSetArray(t *testing.T) {
	env.SetArray("STRING_ARRAY", stringArray)

	var str string
	str += "["
	for _, s := range stringArray {
		str += "\"" + strings.ReplaceAll(s, "\"", "\\\"") + "\", "
	}
	if len(stringArray) > 0 {
		str = str[:len(str)-2]
	}
	str += "]"

	if os.Getenv("STRING_ARRAY") != str {
		t.Fatalf("env var not as expected")
	}
}

func TestSetBoolArray(t *testing.T) {
	arr := make([]bool, len(boolArray))
	for i := range boolArray {
		arr[i] = boolArray[i].Expect
	}

	env.SetBoolArray("BOOL_ARRAY", arr)

	var str string
	str += "["
	for _, test := range boolArray {
		str += strconv.FormatBool(test.Expect) + ", "
	}
	if len(boolArray) > 0 {
		str = str[:len(str)-2]
	}
	str += "]"

	if os.Getenv("BOOL_ARRAY") != str {
		t.Fatalf("env var not as expected")
	}
}

func TestSetInt64Array(t *testing.T) {
	arr := make([]int64, len(int64Array))
	for i := range int64Array {
		arr[i] = int64Array[i].Expect
	}

	env.SetInt64Array("INT64_ARRAY", arr)

	var str string
	str += "["
	for _, test := range int64Array {
		str += strconv.FormatInt(test.Expect, 10) + ", "
	}
	if len(int64Array) > 0 {
		str = str[:len(str)-2]
	}
	str += "]"

	if os.Getenv("INT64_ARRAY") != str {
		t.Fatalf("env var not as expected")
	}
}

func TestSetUint64Array(t *testing.T) {
	arr := make([]uint64, len(uint64Array))
	for i := range uint64Array {
		arr[i] = uint64Array[i].Expect
	}

	env.SetUint64Array("UINT64_ARRAY", arr)

	var str string
	str += "["
	for _, test := range uint64Array {
		str += strconv.FormatUint(test.Expect, 10) + ", "
	}
	if len(uint64Array) > 0 {
		str = str[:len(str)-2]
	}
	str += "]"

	if os.Getenv("UINT64_ARRAY") != str {
		t.Fatalf("env var not as expected")
	}
}

func TestSetFloatArray(t *testing.T) {
	arr := make([]float64, len(floatArray))
	for i := range floatArray {
		arr[i] = floatArray[i].Expect
	}

	env.SetFloatArray("FLOAT_ARRAY", arr)

	var str string
	str += "["
	for _, test := range floatArray {
		str += strconv.FormatFloat(test.Expect, 'f', -1, 64) + ", "
	}
	if len(floatArray) > 0 {
		str = str[:len(str)-2]
	}
	str += "]"

	if os.Getenv("FLOAT_ARRAY") != str {
		t.Fatalf("env var not as expected")
	}
}

func TestSetDurationArray(t *testing.T) {
	arr := make([]time.Duration, len(durationArray))
	for i := range durationArray {
		arr[i] = durationArray[i].Expect
	}

	env.SetDurationArray("DURATION_ARRAY", arr)

	var str string
	str += "["
	for _, test := range durationArray {
		str += test.Expect.String() + ", "
	}
	if len(durationArray) > 0 {
		str = str[:len(str)-2]
	}
	str += "]"

	if os.Getenv("DURATION_ARRAY") != str {
		t.Fatalf("env var not as expected")
	}
}

func TestSetTimeArray(t *testing.T) {
	arr := make([]time.Time, len(timeArray))
	for i := range timeArray {
		arr[i] = timeArray[i].Expect
	}

	env.SetTimeArray("TIME_ARRAY", arr)

	var str string
	str += "["
	for _, test := range timeArray {
		str += "\"" + test.Expect.Format(time.RFC1123) + "\", "
	}
	if len(timeArray) > 0 {
		str = str[:len(str)-2]
	}
	str += "]"

	if os.Getenv("TIME_ARRAY") != str {
		t.Fatalf("env var not as expected")
	}
}

func TestSetSizeArray(t *testing.T) {
	arr := make([]bytesize.Size, len(sizeArray))
	for i := range sizeArray {
		arr[i] = sizeArray[i].Expect
	}

	env.SetSizeArray("SIZE_ARRAY", arr)

	var str string
	str += "["
	for _, test := range sizeArray {
		str += test.Expect.StringIEC() + ", "
	}
	if len(sizeArray) > 0 {
		str = str[:len(str)-2]
	}
	str += "]"

	if os.Getenv("SIZE_ARRAY") != str {
		t.Fatalf("env var not as expected")
	}
}

func parsetime(s string) time.Time {
	t, err := time.Parse(time.RFC1123, s)
	if err != nil {
		panic(err)
	}
	return t
}
